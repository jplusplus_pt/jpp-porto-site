// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

// Animated scroll for the menu
// from https://stackoverflow.com/questions/22064813/jquery-scrollto-on-dynamic-menu-page-sections
$('.menu-item').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr('data-slug');
    $('html,body').animate({
        scrollTop: $('#'+id).offset().top
    }, 1000);
});

// Slick.js image gallery
$(document).ready(function(){
    $('.cooking-slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      accessibility: true,
    });    
    $('.surv-slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      accessibility: true,
    });
    $('.godi-slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      accessibility: true,
    });
    $('.hunger-slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      accessibility: true,
    });
    $('.emailself-slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      accessibility: true,
    });

});
